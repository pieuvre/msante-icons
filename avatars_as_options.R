#' Read Avatar Radio Buttons from folder list
#'
#' @param path path to folder of image files
#'
#' @return saved txt file of radio button specification
#' @export
#'
avatars_as_options <- function(path = "avatars/anim/") {
  av.names <- list.files(path) 
  buttons <- paste0(1:length(av.names),", <img src=\"https://gitlab.com/pieuvre/msante-icons/-/raw/main/avatars/anim/",
        av.names,"\" width=\"100%\">") |> 
    paste(collapse = "\n")
  fileConn <- file("avatar_radio_buttons.txt")
  writeLines(buttons,fileConn)
  close(fileConn)
  cat(paste("\"avatar_radio_buttons.txt\" written to working-directory:",getwd()))
}
