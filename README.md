# mSanté Icons and Images

Project description…

------------------------------------------------------------------------

### Attribution to creators

------------------------------------------------------------------------

<details>
<summary>
<h4>
Openmoji
</h4>
</summary>
<details>
<summary>
<h4>
Openmoji - 12.1
</h4>
</summary>

<div>

Unicode 12.1 emojis designed by
<a href="https://github.com/hfg-gmuend/openmoji/releases/tag/12.1.0" />OpenMoji</a>
– the open-source emoji and icon project. License:
<a href="https://creativecommons.org/licenses/by-sa/4.0/#" />CC BY-SA
4.0</a>

</div>

</br><a title="teacup-without-handle" href="https://openmoji.org/data/color/svg/1F375.svg" ><img src="icons/1F375_teacup-without-handle.svg" alt="teacup-without-handle" width="100px" ></a><a title="hot-beverage" href="https://openmoji.org/data/color/svg/2615.svg" ><img src="icons/2615_hot-beverage.svg" alt="hot-beverage" width="100px" ></a>
</details>
<details>
<summary>
<h4>
Openmoji - 13.1
</h4>
</summary>

<div>

Unicode 13.1 emojis designed by
<a href="https://openmoji.org/" />OpenMoji</a> – the open-source emoji
and icon project. License:
<a href="https://creativecommons.org/licenses/by-sa/4.0/#" />CC BY-SA
4.0</a>

</div>

</br><a title="sun-behind-small-cloud" href="https://openmoji.org/data/color/svg/1F324.svg" ><img src="icons/1F324_sun-behind-small-cloud.svg" alt="sun-behind-small-cloud" width="100px" ></a><a title="door" href="https://openmoji.org/data/color/svg/1F6AA.svg" ><img src="icons/1F324_sun-behind-small-cloud.svg" alt="door" width="100px" ></a><a title="deciduous-tree" href="https://openmoji.org/data/color/svg/1F333.svg" ><img src="icons/1F333_deciduous-tree.svg" alt="deciduous-tree" width="100px" ></a><a title="ear of corn" href="https://openmoji.org/data/color/svg/1F33D.svg" ><img src="icons/1F33D_ear-of-corn.svg" alt="ear of corn" width="100px" ></a><a title="Tomato" href="https://openmoji.org/data/color/svg/1F345.svg" ><img src="icons/1F345_tomato.svg" alt="Tomato" width="100px" ></a><a title="tomato" href="https://openmoji.org/data/color/svg/1F345.svg" ><img src="icons/1F345_tomato.svg" alt="tomato" width="100px" ></a><a title="Grapes" href="https://openmoji.org/data/color/svg/1F347.svg" ><img src="icons/1F347_grapes.svg" alt="Grapes" width="100px" ></a><a title="Watermelon" href="https://openmoji.org/data/color/svg/1F349.svg" ><img src="icons/1F349_watermelon.svg" alt="Watermelon" width="100px" ></a><a title="Tangerine" href="https://openmoji.org/data/color/svg/1F34A.svg" ><img src="icons/1F34A_tangerine.svg" alt="Tangerine" width="100px" ></a><a title="Banana" href="https://openmoji.org/data/color/svg/1F34C.svg" ><img src="icons/1F34C_banana.svg" alt="Banana" width="100px" ></a><a title="banana" href="https://openmoji.org/library/#search=banana&emoji=1F34C" ><img src="icons/1F34C_banana.svg" alt="banana" width="100px" ></a><a title="red-apple" href="https://openmoji.org/library/#search=apple&emoji=1F34E" ><img src="icons/1F34E_red-apple.svg" alt="red-apple" width="100px" ></a><a title="Red  Apple" href="https://openmoji.org/data/color/svg/1F34E.svg" ><img src="icons/1F34E_red-apple.svg" alt="Red  Apple" width="100px" ></a><a title="Pear" href="https://openmoji.org/data/color/svg/1F350.svg" ><img src="icons/1F350_pear.svg" alt="Pear" width="100px" ></a><a title="Peach" href="https://openmoji.org/data/color/svg/1F351.svg" ><img src="icons/1F351_peach.svg" alt="Peach" width="100px" ></a><a title="Strawberry" href="https://openmoji.org/data/color/svg/1F353.svg" ><img src="icons/1F353_strawberry.svg" alt="Strawberry" width="100px" ></a><a title="Pizza" href="https://openmoji.org/data/color/svg/1F355.svg" ><img src="icons/1F355_pizza.svg" alt="Pizza" width="100px" ></a><a title="poultry-leg" href="https://openmoji.org/data/color/svg/1F357.svg" ><img src="icons/1F357_poultry-leg.svg" alt="poultry-leg" width="100px" ></a><a title="Cooked rice" href="https://openmoji.org/data/color/svg/1F35A.svg" ><img src="icons/1F35A_cooked-rice.svg" alt="Cooked rice" width="100px" ></a><a title="curry-rice" href="https://openmoji.org/data/color/svg/1F35B.svg" ><img src="icons/1F35B_curry-rice.svg" alt="curry-rice" width="100px" ></a><a title="Spaghetti" href="https://openmoji.org/data/color/svg/1F35D.svg" ><img src="icons/1F35D_spaghetti.svg" alt="Spaghetti" width="100px" ></a><a title="French Fries" href="https://openmoji.org/data/color/svg/1F35F.svg" ><img src="icons/1F35F_french-fries.svg" alt="French Fries" width="100px" ></a><a title="lollipop" href="https://openmoji.org/data/color/svg/1F36D.svg" ><img src="icons/1F36_lollipop.svg" alt="lollipop" width="100px" ></a><a title="cocktail-glass" href="https://openmoji.org/data/color/svg/1F378.svg" ><img src="icons/1F378_cocktail-glass.svg" alt="cocktail-glass" width="100px" ></a><a title="tropical-drink" href="https://openmoji.org/data/color/svg/1F379.svg" ><img src="icons/1F379_tropical-drink.svg" alt="tropical-drink" width="100px" ></a><a title="beer-mug" href="https://openmoji.org/data/color/svg/1F37A.svg" ><img src="icons/1F37A_beer-mug.svg" alt="beer-mug" width="100px" ></a><a title="performing-arts" href="https://openmoji.org/data/color/svg/1F3AD.svg" ><img src="icons/1F3AD_performing-arts.svg" alt="performing-arts" width="100px" ></a><a title="musical-note" href="https://openmoji.org/data/color/svg/1F3B5.svg" ><img src="icons/1F3B5_musical-note.svg" alt="musical-note" width="100px" ></a><a title="basketball" href="https://openmoji.org/data/color/svg/1F3C0.svg" ><img src="icons/1F3C0_basketball.svg" alt="basketball" width="100px" ></a><a title="person-lifting-weights" href="https://openmoji.org/data/color/svg/1F3CB.svg" ><img src="icons/1F3CB_person-lifting-weights.svg" alt="person-lifting-weights" width="100px" ></a><a title="houses" href="https://openmoji.org/data/color/svg/1F3D8.svg" ><img src="icons/1F3D8_houses.svg" alt="houses" width="100px" ></a><a title="school" href="https://openmoji.org/data/color/svg/1F3EB.svg" ><img src="icons/1F3D8_houses.svg" alt="school" width="100px" ></a><a title="house" href="https://openmoji.org/data/color/svg/1F3E0.svg" ><img src="icons/1F3E0_house.svg" alt="house" width="100px" ></a><a title="hospital" href="https://openmoji.org/data/color/svg/1F3E5.svg" ><img src="icons/1F3E5_hospital.svg" alt="hospital" width="100px" ></a><a title="convenience-store" href="https://openmoji.org/data/color/svg/1F3EA.svg" ><img src="icons/1F3EA_convenience-store.svg" alt="convenience-store" width="100px" ></a><a title="department-store" href="https://openmoji.org/data/color/svg/1F3EC.svg" ><img src="icons/1F3EC_department-store.svg" alt="department-store" width="100px" ></a><a title="snail" href="https://openmoji.org/data/color/svg/1F40C.svg" ><img src="icons/1F40C_snail.svg" alt="snail" width="100px" ></a><a title="dog-face" href="https://openmoji.org/data/color/svg/1F436.svg" ><img src="icons/1F436_dog-face.svg" alt="dog-face" width="100px" ></a><a title="bust-in-silhouette" href="https://openmoji.org/data/color/svg/1F464.svg" ><img src="icons/1F464_bust-in-silhouette.svg" alt="bust-in-silhouette" width="100px" ></a><a title="family" href="https://openmoji.org/data/color/svg/1F46A.svg" ><img src="icons/1F468_family.svg" alt="family" width="100px" ></a><a title="family-man-woman-girl-boy" href="https://openmoji.org/data/color/svg/1F468-200D-1F469-200D-1F467-200D-1F466.svg" ><img src="icons/1F468-200D-1F469-200D-1F467-200D-1F466_family-man-woman-girl-boy.svg" alt="family-man-woman-girl-boy" width="100px" ></a><a title="woman-office-worker" href="https://openmoji.org/data/color/svg/1F469-200D-1F4BC.svg" ><img src="icons/1F469-200D-1F4BC_woman-office-worker.svg" alt="woman-office-worker" width="100px" ></a><a title="woman-dancing" href="https://openmoji.org/data/color/svg/1F483.svg" ><img src="icons/1F483_woman-dancing.svg" alt="woman-dancing" width="100px" ></a><a title="purple-heart" href="https://openmoji.org/data/color/svg/1F49C.svg" ><img src="icons/1F49C_purple-heart.svg" alt="purple-heart" width="100px" ></a><a title="though-balloon" href="https://openmoji.org/data/color/svg/1F4AD.svg" ><img src="icons/1F4AD_thought-balloon.svg" alt="though-balloon" width="100px" ></a><a title="calendar" href="https://openmoji.org/library/#search=calendar&emoji=1F4C5" ><img src="icons/1F4C5_calendar.svg" alt="calendar" width="100px" ></a><a title="telephone-receiver" href="https://openmoji.org/data/color/svg/1F4DE.svg" ><img src="icons/1F4DE_telephone-receiver.svg" alt="telephone-receiver" width="100px" ></a><a title="television" href="https://openmoji.org/data/color/svg/1F4FA.svg" ><img src="icons/1F4FA_television.svg" alt="television" width="100px" ></a><a title="counterclockwise-arrow-button" href="https://openmoji.org/library/#search=1F504&emoji=1F504" ><img src="icons/1F504_counterclockwise-arrows-button.svg" alt="counterclockwise-arrow-button" width="100px" ></a><a title="battery" href="https://openmoji.org/data/color/svg/1F50B.svg" ><img src="icons/1F50B_battery.svg" alt="battery" width="100px" ></a><a title="red-triangle-pointed-up" href="https://openmoji.org/data/color/svg/1F53A.svg" ><img src="icons/1F53A_red-triangle-pointed-up.svg" alt="red-triangle-pointed-up" width="100px" ></a><a title="nine o’clock" href="https://openmoji.org/data/color/svg/1F558.svg" ><img src="icons/1F558_nine-oclock.svg" alt="nine o’clock" width="100px" ></a><a title="speaking-head" href="https://openmoji.org/data/color/svg/1F5E3.svg" ><img src="icons/1F5E3_speaking-head.svg" alt="speaking-head" width="100px" ></a><a title="grinning-face" href="https://openmoji.org/data/color/svg/1F600.svg" ><img src="icons/1F600_grinning-face.svg" alt="grinning-face" width="100px" ></a><a title="grinning-face-with-smiling-eyes" href="https://openmoji.org/data/color/svg/1F604.svg" ><img src="icons/1F604_grinning-face-with-smiling-eyes.svg" alt="grinning-face-with-smiling-eyes" width="100px" ></a><a title="winking-face" href="https://openmoji.org/data/color/svg/1F609.svg" ><img src="icons/1F609_winking-face.svg" alt="winking-face" width="100px" ></a><a title="smiling-face-with-smiling-eyes" href="https://openmoji.org/data/color/svg/1F642.svg" ><img src="icons/1F60A_smiling-face-with-smiling-eyes.svg" alt="smiling-face-with-smiling-eyes" width="100px" ></a><a title="relieved-face" href="https://openmoji.org/data/color/svg/1F60C.svg" ><img src="icons/1F60C_relieved-face.svg" alt="relieved-face" width="100px" ></a><a title="relieved-face" href="https://openmoji.org/data/color/svg/1F60C.svg" ><img src="icons/1F60C_relieved-face.svg" alt="relieved-face" width="100px" ></a><a title="smirking-face" href="https://openmoji.org/library/#search=1F60F&emoji=1F60F" ><img src="icons/1F60F_smirking-face.svg" alt="smirking-face" width="100px" ></a><a title="neutral-face" href="https://openmoji.org/data/color/svg/1F610.svg" ><img src="icons/1F610_neutral-face.svg" alt="neutral-face" width="100px" ></a><a title="unamused-face" href="https://openmoji.org/library/#search=1F612" ><img src="icons/1F612_unamused-face.svg" alt="unamused-face" width="100px" ></a><a title="pensive-face" href="https://openmoji.org/data/color/svg/1F614.svg" ><img src="icons/1F614_pensive-face.svg" alt="pensive-face" width="100px" ></a><a title="pouting-face" href="https://openmoji.org/data/color/svg/1F621.svg" ><img src="icons/1F621_pouting-face.svg" alt="pouting-face" width="100px" ></a><a title="grimacing-face" href="https://openmoji.org/data/color/svg/1F62C.svg" ><img src="icons/1F62C_grimacing-face.svg" alt="grimacing-face" width="100px" ></a><a title="face-exhaling" href="https://openmoji.org/library/#search=1F62E-200D-1F4A8&emoji=1F62E-200D-1F4A8" ><img src="icons/1F62E-200D-1F4A8_face-exhaling.svg" alt="face-exhaling" width="100px" ></a><a title="anxious-face-with-sweat" href="https://openmoji.org/data/color/svg/1F630.svg" ><img src="icons/1F630_anxious-face-with-sweat.svg" alt="anxious-face-with-sweat" width="100px" ></a><a title="face-screaming-in-fear" href="https://openmoji.org/data/color/svg/1F631.svg" ><img src="icons/1F631_face-screaming-in-fear.svg" alt="face-screaming-in-fear" width="100px" ></a><a title="sleeping-face" href="https://openmoji.org/data/color/svg/1F634.svg" ><img src="icons/1F634_sleeping-face.svg" alt="sleeping-face" width="100px" ></a><a title="face-with-spiral-eyes" href="https://openmoji.org/data/color/svg/1F635-200D-1F4AB.svg" ><img src="icons/1F635-200D-1F4AB_face-with-spiral-eyes.svg" alt="face-with-spiral-eyes" width="100px" ></a><a title="face-in-clouds" href="https://openmoji.org/library/#search=1F636-200D-1F32B-FE0F" ><img src="icons/1F636-200D-1F32B-FE0F_face-in-clouds.svg" alt="face-in-clouds" width="100px" ></a><a title="slightly-smiling-face" href="https://openmoji.org/data/color/svg/1F642.svg" ><img src="icons/1F642_slightly-smiling-face.svg" alt="slightly-smiling-face" width="100px" ></a><a title="man-gesturing-ok" href="https://openmoji.org/data/color/svg/1F646-200D-2642-FE0F.svg" ><img src="icons/1F646-200D-2642-FE0F_man-gesturing-ok.svg" alt="man-gesturing-ok" width="100px" ></a><a title="minibus" href="https://openmoji.org/data/color/svg/1F690.svg" ><img src="icons/1F690_minibus.svg" alt="minibus" width="100px" ></a><a title="automobile" href="https://openmoji.org/data/color/svg/1F697.svg" ><img src="icons/1F697_automobile.svg" alt="automobile" width="100px" ></a><a title="cigarette" href="https://openmoji.org/data/color/svg/1F6AC.svg" ><img src="icons/1F6AC_cigarette.svg" alt="cigarette" width="100px" ></a><a title="couch-and-lamp" href="https://openmoji.org/data/color/svg/1F6CB.svg" ><img src="icons/1F6CB_couch-and-lamp.svg" alt="couch-and-lamp" width="100px" ></a><a title="person-in-bed" href="https://openmoji.org/data/color/svg/1F6CC.svg" ><img src="icons/1F6CC_person-in-bed.svg" alt="person-in-bed" width="100px" ></a><a title="shopping-cart" href="https://openmoji.org/data/color/svg/1F6D2.svg" ><img src="icons/1F6D2shopping-cart.svg" alt="shopping-cart" width="100px" ></a><a title="blue-square" href="https://openmoji.org/data/color/svg/1F7E6.svg" ><img src="icons/1F7E6_blue-square.svg" alt="blue-square" width="100px" ></a><a title="thinking-face" href="https://openmoji.org/library/#search=1F914&emoji=1F914" ><img src="icons/1F914_thinking-face.svg" alt="thinking-face" width="100px" ></a><a title="handshake" href="https://openmoji.org/data/color/svg/1F91D.svg" ><img src="icons/1F91D_handshake.svg" alt="handshake" width="100px" ></a><a title="star-struck" href="https://openmoji.org/data/color/svg/1F929.svg" ><img src="icons/1F929_star-struck.svg" alt="star-struck" width="100px" ></a><a title="face-with-symbols-on-mouth" href="https://openmoji.org/data/color/svg/1F92C.svg" ><img src="icons/1F92C_face-with-symbols-on-mouth.svg" alt="face-with-symbols-on-mouth" width="100px" ></a><a title="person-cartwheeling" href="https://openmoji.org/data/color/svg/1F938.svg" ><img src="icons/1F938_person-cartwheeling.svg" alt="person-cartwheeling" width="100px" ></a><a title="clinking-glasses" href="https://openmoji.org/data/color/svg/1F942.svg" ><img src="icons/1F942_clinking-glasses.svg" alt="clinking-glasses" width="100px" ></a><a title="tumbler-glass" href="https://openmoji.org/data/color/svg/1F943.svg" ><img src="icons/1F943_tumbler-glass.svg" alt="tumbler-glass" width="100px" ></a><a title="croissant" href="https://openmoji.org/data/color/svg/1F950.svg" ><img src="icons/1F950_croissant.svg" alt="croissant" width="100px" ></a><a title="Potato" href="https://openmoji.org/data/color/svg/1F954.svg" ><img src="icons/1F954_potato.svg" alt="Potato" width="100px" ></a><a title="carrot" href="https://openmoji.org/data/color/svg/1F955.svg" ><img src="icons/1F955_carrot.svg" alt="carrot" width="100px" ></a><a title="baguette-bread" href="https://openmoji.org/data/color/svg/1F956.svg" ><img src="icons/1F956_baguette-bread.svg" alt="baguette-bread" width="100px" ></a><a title="green salad" href="https://openmoji.org/data/color/svg/1F957.svg" ><img src="icons/1F957_green-salad.svg" alt="green salad" width="100px" ></a><a title="Egg" href="https://openmoji.org/data/color/svg/1F95A.svg" ><img src="icons/1F95A_egg.svg" alt="Egg" width="100px" ></a><a title="glas-of-milk" href="https://openmoji.org/data/color/svg/1F95B.svg" ><img src="icons/1F95B_glass-of-milk.svg" alt="glas-of-milk" width="100px" ></a><a title="Kiwi" href="https://openmoji.org/data/color/svg/1F95D.svg" ><img src="icons/1F95D_kiwi-fruit.svg" alt="Kiwi" width="100px" ></a><a title="kiwi" href="https://openmoji.org/data/color/svg/1F95D.svg" ><img src="icons/1F95D_kiwi-fruit.svg" alt="kiwi" width="100px" ></a><a title="cup-with-straw" href="https://openmoji.org/data/color/svg/1F964.svg" ><img src="icons/1F964_cup-with-straw.svg" alt="cup-with-straw" width="100px" ></a><a title="broccoli (Openmoji)" href="https://openmoji.org/data/color/svg/1F966.svg" ><img src="icons/1F966_broccoli.svg" alt="broccoli (Openmoji)" width="100px" ></a><a title="pie" href="https://openmoji.org/data/color/svg/1F967.svg" ><img src="icons/1F967_pie.svg" alt="pie" width="100px" ></a><a title="cut-of-meat" href="https://openmoji.org/data/color/svg/1F969.svg" ><img src="icons/1F969_cut-of-meat.svg" alt="cut-of-meat" width="100px" ></a><a title="sandwich" href="https://openmoji.org/data/color/svg/1F96A.svg" ><img src="icons/1F96A_sandwich.svg" alt="sandwich" width="100px" ></a><a title="canned food" href="https://openmoji.org/data/color/svg/1F96B.svg" ><img src="icons/1F96Bcanned-food.svg" alt="canned food" width="100px" ></a><a title="shrimp" href="https://openmoji.org/library/#search=1F990&emoji=1F990" ><img src="icons/1F990_shrimp.svg" alt="shrimp" width="100px" ></a><a title="cheese-wedge" href="https://openmoji.org/data/color/svg/1F9C0.svg" ><img src="icons/1F9C0_cheese-wedge.svg" alt="cheese-wedge" width="100px" ></a><a title="salt" href="https://openmoji.org/data/color/svg/1F9C2.svg" ><img src="icons/1F9C2_salt.svg" alt="salt" width="100px" ></a><a title="beverage_box" href="https://openmoji.org/data/color/svg/1F9C3.svg" ><img src="icons/1F9C3_beverage-box.svg" alt="beverage_box" width="100px" ></a><a title="person-standing" href="https://openmoji.org/data/color/svg/1F9CD.svg" ><img src="icons/1F9CD_person-standing.svg" alt="person-standing" width="100px" ></a><a title="person-standing-white" href="https://openmoji.org/data/black/svg/1F9CD-200D-2642-FE0F.svg" ><img src="icons/1F9CD-200D-2642-FE0F_man-standing.svg" alt="person-standing-white" width="100px" ></a><a title="face-with-monocle" href="https://openmoji.org/data/color/svg/1F9D0.svg" ><img src="icons/1F9D0_face-with-monocle.svg" alt="face-with-monocle" width="100px" ></a><a title="person" href="https://openmoji.org/data/color/svg/1F9D1.svg" ><img src="icons/1F9D1_person.svg" alt="person" width="100px" ></a><a title="man-in-lotus-position" href="https://openmoji.org/data/color/svg/1F9D8-200D-2642-FE0F.svg" ><img src="icons/1F9D8-200D-2642-FE0F_man-in-lotus-position.svg" alt="man-in-lotus-position" width="100px" ></a><a title="compass" href="https://openmoji.org/library/#search=find&emoji=1F9ED" ><img src="icons/1F9ED_compass.svg" alt="compass" width="100px" ></a><a title="broom" href="https://openmoji.org/data/color/svg/1F9F9.svg" ><img src="icons/1F9F9_broom.svg" alt="broom" width="100px" ></a><a title="soap" href="https://openmoji.org/data/color/svg/1F9FC.svg" ><img src="icons/1F9FC_soap.svg" alt="soap" width="100px" ></a><a title="blueberry" href="https://openmoji.org/data/color/svg/1FAD0.svg" ><img src="icons/1FAD0_blueberries.svg" alt="blueberry" width="100px" ></a><a title="fuondue" href="https://openmoji.org/data/color/svg/1FAD5.svg" ><img src="icons/1FAD5_fondue.svg" alt="fuondue" width="100px" ></a><a title="drowning-face" href="https://openmoji.org/data/color/svg/2639.svg" ><img src="icons/2639_frowning-face.svg" alt="drowning-face" width="100px" ></a><a title="high-voltage" href="https://openmoji.org/data/color/svg/26A1.svg" ><img src="icons/26A1_high-voltage.svg" alt="high-voltage" width="100px" ></a><a title="white-circle" href="https://openmoji.org/data/color/svg/26AA.svg" ><img src="icons/26AA_white-circle.svg" alt="white-circle" width="100px" ></a><a title="cross_mark" href="https://openmoji.org/data/black/svg/274C.svg" ><img src="icons/274C_cross-mark.svg" alt="cross_mark" width="100px" ></a><a title="question-mark" href="https://openmoji.org/data/color/svg/2754.svg" ><img src="icons/2754_question-mark.svg" alt="question-mark" width="100px" ></a><a title="white-large-square" href="https://openmoji.org/data/color/svg/2B1C.svg" ><img src="icons/2B1C_white-large-square.svg" alt="white-large-square" width="100px" ></a><a title="person-running" href="https://openmoji.org/data/color/svg/1F3C3.svg" ><img src="icons/1F3C3_person-running.svg" alt="person-running" width="100px" ></a><a title="wine-glass" href="https://openmoji.org/data/color/svg/1F377.svg" ><img src="icons/caffeine.png" alt="wine-glass" width="100px" ></a><a title="goldfish" href="https://openmoji.org/data/color/svg/E000.svg" ><img src="icons/E000_goldfish.svg" alt="goldfish" width="100px" ></a><a title="boule bread" href="https://openmoji.org/data/color/svg/E0CA.svg" ><img src="icons/E0CA_boule-bread.svg" alt="boule bread" width="100px" ></a><a title="person-with-dog" href="https://openmoji.org/data/color/svg/E182.svg" ><img src="icons/E182_person-with-dog.svg" alt="person-with-dog" width="100px" ></a><a title="raspberry" href="https://openmoji.org/data/color/svg/E1C9.svg" ><img src="icons/E1C9_raspberry-pi.svg" alt="raspberry" width="100px" ></a><a title="cafeteria" href="https://openmoji.org/data/color/svg/E200.svg" ><img src="icons/E200_cafeteria.svg" alt="cafeteria" width="100px" ></a><a title="exhausted-face" href="https://openmoji.org/data/color/svg/E280.svg" ><img src="icons/E280_exhausted-face.svg" alt="exhausted-face" width="100px" ></a><a title="Orange" href="https://openmoji.org/data/color/svg/1F34A.svg" ><img src="icons/1F34A_orange.svg" alt="Orange" width="100px" ></a>
</details>
</details>
<details>
<summary>
<h4>
NIMH
</h4>
</summary>
<details>
<summary>
<h4>
NIMH
</h4>
</summary>

<div>

Icons derived from other creators by the
<a href="https://github.com/ChildMindInstitute">Child Mind Insitute</a>
for the
<a href="https://github.com/ChildMindInstitute/NIMH_EMA_applet">HBN EMA
Mindlogger Applet</a> with NIMH content.

</div>

</br><a title="redCircle" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/1F534.png" ><img src="icons/1F534redCircle.png" alt="redCircle" width="100px" ></a><a title="greenCircle" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/1F7E2.png" ><img src="icons/1F7E2greenCircle.png" alt="greenCircle" width="100px" ></a><a title="whiteCircle" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/26AA.png" ><img src="icons/26AAwhiteCircle.png" alt="whiteCircle" width="100px" ></a><a title="activityLight" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/activityLight.png" ><img src="icons/activityLight.png" alt="activityLight" width="100px" ></a><a title="activityVigorous" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/activityVigorous.png" ><img src="icons/activityVigorous.png" alt="activityVigorous" width="100px" ></a><a title="alarmClock" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/alarmClock.png" ><img src="icons/alarmClock.png" alt="alarmClock" width="100px" ></a><a title="allFood" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/allFood.png" ><img src="icons/allFood.png" alt="allFood" width="100px" ></a><a title="allFoodTime" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/allFoodTime.png" ><img src="icons/allFoodTime.png" alt="allFoodTime" width="100px" ></a><a title="allFoodTime" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/allFoodTime.png" ><img src="icons/allFoodTime.png" alt="allFoodTime" width="100px" ></a><a title="allSocialMedia" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/allSocialMedia.png" ><img src="icons/allSocialMedia.png" alt="allSocialMedia" width="100px" ></a><a title="bedClock" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/bedClock.png" ><img src="icons/bedClock.png" alt="bedClock" width="100px" ></a><a title="caffeine" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/caffeine.png" ><img src="icons/caffeine.png" alt="caffeine" width="100px" ></a><a title="closedEyeClock" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/closedEyeClock.png" ><img src="icons/closedEyeClock.png" alt="closedEyeClock" width="100px" ></a><a title="dayNight" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/dayNight.png" ><img src="icons/dayNight.png" alt="dayNight" width="100px" ></a><a title="lowBattery" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/lowBattery.png" ><img src="icons/lowBattery.png" alt="lowBattery" width="100px" ></a><a title="snack" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/snack.png" ><img src="icons/snack.png" alt="snack" width="100px" ></a><a title="water" href="https://raw.githubusercontent.com/hotavocado/HBN_EMA_NIMH2/master/images/new/water.png" ><img src="icons/water.png" alt="water" width="100px" ></a>
</details>
</details>
<details>
<summary>
<h4>
flaticons
</h4>
</summary>
<details>
<summary>
<h4>
aranagraphics
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/aranagraphics" title="Aranagraphics">Aranagraphics</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="pellegrino-water" href="https://cdn-icons.flaticon.com/png/512/2745/premium/2745112.png?token=exp=1642157196~hmac=2bdd2a1f0c149dcf222a642be5d600c5" ><img src="icons/pellegrino-water.png" alt="pellegrino-water" width="100px" ></a><a title="water-bottle" href="https://cdn-icons.flaticon.com/png/512/2745/premium/2745099.png?token=exp=1642157455~hmac=9ed02dd03d123e3e0f3a27d5e28a2017" ><img src="icons/water-bottle.png" alt="water-bottle" width="100px" ></a>
</details>
<details>
<summary>
<h4>
dDara
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/ddara" title="dDara">dDara</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="chocolate" href="https://cdn-icons-png.flaticon.com/512/3465/3465221.png" ><img src="icons/chocolate.png" alt="chocolate" width="100px" ></a>
</details>
<details>
<summary>
<h4>
DinosoftLabs
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/dinosoftlabs" title="DinosoftLabs">DinosoftLabs</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="salad-dressing" href="https://cdn-icons-png.flaticon.com/512/575/575325.png" ><img src="icons/575325_salad-dressing.png" alt="salad-dressing" width="100px" ></a>
</details>
<details>
<summary>
<h4>
Eucalyp
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/eucalyp" title="Eucalyp">Eucalyp</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="goose" href="https://cdn-icons-png.flaticon.com/512/1462/1462015.png" ><img src="icons/1462015_goose.png" alt="goose" width="100px" ></a><a title="quiche" href="https://cdn-icons-png.flaticon.com/512/1743/1743602.png" ><img src="icons/1743602_quiche.png" alt="quiche" width="100px" ></a><a title="clam" href="https://cdn-icons-png.flaticon.com/512/2243/2243590.png" ><img src="icons/2243590_clam.png" alt="clam" width="100px" ></a><a title="behavior" href="https://cdn-icons-png.flaticon.com/512/5284/5284298.png" ><img src="icons/behavior.png" alt="behavior" width="100px" ></a><a title="mobile-phone" href="https://cdn-icons.flaticon.com/png/512/2974/premium/2974103.png?token=exp=1640093429~hmac=b5260fe8b9463b3583a41e6277c796f9" ><img src="icons/mobile-phone.png" alt="mobile-phone" width="100px" ></a><a title="stomach-pain" href="https://cdn-icons-png.flaticon.com/512/2328/2328364.png" ><img src="icons/stomach-pain.png" alt="stomach-pain" width="100px" ></a>
</details>
<details>
<summary>
<h4>
Flat Icons
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/flat-icons" title="Flat Icons">Flat
Icons</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="noon" href="https://cdn-icons.flaticon.com/png/512/5433/premium/5433477.png?token=exp=1642157088~hmac=a1dac473f0fc7d5fc64aed573518b18c" ><img src="icons/5433477noon.png" alt="noon" width="100px" ></a>
</details>
<details>
<summary>
<h4>
Freepik
</h4>
</summary>

<div>

Icons made by
<a href="https://www.freepik.com" title="Freepik">Freepik</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="Tomato sauce" href="https://cdn-icons-png.flaticon.com/512/1046/1046790.png" ><img src="icons/1046790_tomato-sauce.png" alt="Tomato sauce" width="100px" ></a><a title="sausage" href="https://cdn-icons-png.flaticon.com/512/1070/1070815.png" ><img src="icons/1070815_sausage.png" alt="sausage" width="100px" ></a><a title="cauliflower" href="https://cdn-icons-png.flaticon.com/512/1135/1135481.png" ><img src="icons/1135481_cauliflower.png" alt="cauliflower" width="100px" ></a><a title="pie-slice" href="https://cdn-icons-png.flaticon.com/512/1182/1182200.png" ><img src="icons/1182200_pie-slice.png" alt="pie-slice" width="100px" ></a><a title="Plum" href="https://cdn-icons-png.flaticon.com/512/1514/1514970.png" ><img src="icons/1514970_plum.png" alt="Plum" width="100px" ></a><a title="charcuterie" href="https://cdn-icons-png.flaticon.com/512/1582/1582199.png" ><img src="icons/1582199_charcuterie.png" alt="charcuterie" width="100px" ></a><a title="Tofu" href="https://cdn-icons-png.flaticon.com/512/1625/1625071.png" ><img src="icons/1625071_tofu.png" alt="Tofu" width="100px" ></a><a title="Strawberry Jam" href="https://cdn-icons-png.flaticon.com/512/1888/1888785.png" ><img src="icons/1888785_strawberry-jam.png" alt="Strawberry Jam" width="100px" ></a><a title="ham" href="https://cdn-icons-png.flaticon.com/512/2403/2403242.png" ><img src="icons/2403242_ham.png" alt="ham" width="100px" ></a><a title="network" href="https://cdn-icons-png.flaticon.com/512/2622/2622191.png" ><img src="icons/2622191_network.png" alt="network" width="100px" ></a><a title="headache" href="https://cdn-icons-png.flaticon.com/512/2660/2660210.png" ><img src="icons/2660210_headache.png" alt="headache" width="100px" ></a><a title="Thin soup" href="https://cdn-icons-png.flaticon.com/512/2769/2769929.png" ><img src="icons/2769929_thin-soup.png" alt="Thin soup" width="100px" ></a><a title="cereals-pack" href="https://cdn-icons-png.flaticon.com/512/2829/2829840.png" ><img src="icons/2829840_cereals-pack.png" alt="cereals-pack" width="100px" ></a><a title="liver" href="https://cdn-icons-png.flaticon.com/512/286/286513.png" ><img src="icons/286513_liver.png" alt="liver" width="100px" ></a><a title="afternoon" href="https://cdn-icons.flaticon.com/png/512/2892/premium/2892332.png?token=exp=1642157076~hmac=e4ec9e72c4c99fdfe388c992c5adcd53" ><img src="icons/2892332afternoon.png" alt="afternoon" width="100px" ></a><a title="Fusilli" href="https://cdn-icons-png.flaticon.com/512/2963/2963246.png" ><img src="icons/2963246_fussili.png" alt="Fusilli" width="100px" ></a><a title="pasta" href="https://cdn-icons-png.flaticon.com/512/2963/2963246.png" ><img src="icons/2963246_pasta.png" alt="pasta" width="100px" ></a><a title="cervelas" href="https://cdn-icons-png.flaticon.com/512/3082/3082054.png" ><img src="icons/3082054_cervelas.png" alt="cervelas" width="100px" ></a><a title="yoghurt-fruit" href="https://cdn-icons-png.flaticon.com/512/3142/3142859.png" ><img src="icons/3142859_yoghurt-fruit.png" alt="yoghurt-fruit" width="100px" ></a><a title="chicken-alive" href="https://cdn-icons-png.flaticon.com/512/347/347616.png" ><img src="icons/347616_chicken-alive.png" alt="chicken-alive" width="100px" ></a><a title="yoghurt-nature" href="https://cdn-icons-png.flaticon.com/512/3523/3523405.png" ><img src="icons/3523405_yoghurt-nature.png" alt="yoghurt-nature" width="100px" ></a><a title="spinach" href="https://cdn-icons-png.flaticon.com/512/3523/3523477.png" ><img src="icons/3523477_spinach.png" alt="spinach" width="100px" ></a><a title="pork" href="https://cdn-icons-png.flaticon.com/512/3554/3554771.png" ><img src="icons/3554771_pork.png" alt="pork" width="100px" ></a><a title="zero" href="https://cdn-icons-png.flaticon.com/512/3570/3570095.png" ><img src="icons/3570095_zero.png" alt="zero" width="100px" ></a><a title="two" href="https://cdn-icons-png.flaticon.com/512/3570/3570100.png" ><img src="icons/3570100_two.png" alt="two" width="100px" ></a><a title="biscotte" href="https://cdn-icons-png.flaticon.com/512/3595/3595626.png" ><img src="icons/3595626_biscotte.png" alt="biscotte" width="100px" ></a><a title="tuna " href="https://cdn-icons-png.flaticon.com/512/372/372123.png" ><img src="icons/372123_tuna.png" alt="tuna " width="100px" ></a><a title="sleepy-2" href="https://cdn-icons-png.flaticon.com/512/4126/4126243.png" ><img src="icons/4126243_sleepy.png" alt="sleepy-2" width="100px" ></a><a title="olive oil" href="https://cdn-icons-png.flaticon.com/512/4264/4264676.png" ><img src="icons/4264676_olive-oil.png" alt="olive oil" width="100px" ></a><a title="apricot" href="https://cdn-icons-png.flaticon.com/512/4264/4264731.png" ><img src="icons/4264731_apricot.png" alt="apricot" width="100px" ></a><a title="clementine" href="https://cdn-icons-png.flaticon.com/512/4373/4373996.png" ><img src="icons/4373996_clementine.png" alt="clementine" width="100px" ></a><a title="yoghurt-light" href="https://cdn-icons-png.flaticon.com/512/4391/4391600.png" ><img src="icons/4391600_yoghurt-light.png" alt="yoghurt-light" width="100px" ></a><a title="calf" href="https://cdn-icons-png.flaticon.com/512/4594/4594681.png" ><img src="icons/4594681_calf.png" alt="calf" width="100px" ></a><a title="Couscous" href="https://cdn-icons-png.flaticon.com/512/5091/5091140.png" ><img src="icons/5091140_couscous.png" alt="Couscous" width="100px" ></a><a title="broccoli" href="https://cdn-icons-png.flaticon.com/512/5346/5346060.png" ><img src="icons/5346060_broccoli.png" alt="broccoli" width="100px" ></a><a title="green bean" href="https://cdn-icons-png.flaticon.com/512/5346/5346633.png" ><img src="icons/5346633_grean-bean.png" alt="green bean" width="100px" ></a><a title="green pea" href="https://cdn-icons-png.flaticon.com/512/5638/5638731.png" ><img src="icons/5638731_green-pea.png" alt="green pea" width="100px" ></a><a title="apple-jam" href="https://cdn-icons-png.flaticon.com/512/600/600486.png" ><img src="icons/600486_apple-jam.png" alt="apple-jam" width="100px" ></a><a title="cracker" href="https://cdn-icons-png.flaticon.com/512/6271/6271415.png" ><img src="icons/6271415_cracker.png" alt="cracker" width="100px" ></a><a title="Potatos" href="https://cdn-icons-png.flaticon.com/512/933/933248.png" ><img src="icons/933248_potatos.png" alt="Potatos" width="100px" ></a><a title="Abricot" href="https://cdn-icons-png.flaticon.com/512/4264/4264731.png" ><img src="icons/abricot.png" alt="Abricot" width="100px" ></a><a title="alcohol-drinks" href="https://cdn-icons-png.flaticon.com/512/920/920541.png" ><img src="icons/alcohol-drinks.png" alt="alcohol-drinks" width="100px" ></a><a title="allergy" href="https://cdn-icons-png.flaticon.com/512/1581/1581770.png" ><img src="icons/allergy.png" alt="allergy" width="100px" ></a><a title="anxiety" href="https://cdn-icons-png.flaticon.com/512/4899/4899568.png" ><img src="icons/anxiety.png" alt="anxiety" width="100px" ></a><a title="breakfast" href="https://cdn-icons.flaticon.com/png/512/1652/premium/1652097.png?token=exp=1642156754~hmac=33ccec1dd8b3b8df906b702a9c799b4b" ><img src="icons/breakfast.png" alt="breakfast" width="100px" ></a><a title="burger" href="https://cdn-icons.flaticon.com/png/512/2403/premium/2403359.png?token=exp=1642158439~hmac=5a3bc9e7c8b324e557277e8188e0244a" ><img src="icons/burger.png" alt="burger" width="100px" ></a><a title="Canneloni" href="https://cdn-icons.flaticon.com/png/512/4465/premium/4465502.png?token=exp=1642158493~hmac=379339291772012861a7319926459c6e" ><img src="icons/cannelloni.png" alt="Canneloni" width="100px" ></a><a title="cereals" href="https://cdn-icons.flaticon.com/png/512/381/premium/381033.png?token=exp=1642158694~hmac=d2b1c33880e7a258ac9aba71fcb14806" ><img src="icons/cereals.png" alt="cereals" width="100px" ></a><a title="cheeses" href="https://cdn-icons.flaticon.com/png/512/1054/premium/1054930.png?token=exp=1642158720~hmac=9de60d148ff5f4f482af6169ec4dc493" ><img src="icons/cheeses.png" alt="cheeses" width="100px" ></a><a title="chicken-skin" href="https://cdn-icons.flaticon.com/png/512/3437/premium/3437541.png?token=exp=1642159092~hmac=5fc98b7bd09ae5df80614b18fc587484" ><img src="icons/chicken-skin.png" alt="chicken-skin" width="100px" ></a><a title="closedEye" href="https://raw.githubusercontent.com/ChildMindInstitute/NIMH_EMA_applet/master/imageAttribution/closedEye.svg" ><img src="icons/closedEye.svg" alt="closedEye" width="100px" ></a><a title="cookie" href="https://cdn-icons.flaticon.com/png/512/1330/premium/1330387.png?token=exp=1642157925~hmac=e083de57d13e8fdd9c0bbcd9f7e47645" ><img src="icons/cookie.png" alt="cookie" width="100px" ></a><a title="cupcake" href="https://cdn-icons.flaticon.com/png/512/737/premium/737980.png?token=exp=1642157900~hmac=a3adb781b572656099a3cc381fe20e88" ><img src="icons/cupcake.png" alt="cupcake" width="100px" ></a><a title="dossier_medical" href="https://cdn-icons-png.flaticon.com/512/1430/1430813.png" ><img src="icons/dossier-medical.png" alt="dossier_medical" width="100px" ></a><a title="emotion" href="https://cdn-icons-png.flaticon.com/512/3062/3062575.png" ><img src="icons/emotion.png" alt="emotion" width="100px" ></a><a title="feta" href="https://cdn-icons.flaticon.com/png/512/1880/premium/1880463.png?token=exp=1642159136~hmac=30b927b80e2a9d94422e591cda89e749" ><img src="icons/feta.png" alt="feta" width="100px" ></a><a title="honey" href="https://cdn-icons.flaticon.com/png/512/2363/premium/2363958.png?token=exp=1642157541~hmac=8b9b08f38f16aa97ab8d2e543dbd383e" ><img src="icons/honey.png" alt="honey" width="100px" ></a><a title="ice-cream" href="https://cdn-icons-png.flaticon.com/512/938/938063.png" ><img src="icons/ice-cream.png" alt="ice-cream" width="100px" ></a><a title="milk-creme" href="https://cdn-icons-png.flaticon.com/512/3500/3500270.png" ><img src="icons/milk-creme.png" alt="milk-creme" width="100px" ></a><a title="milk-creme-with-coffe" href="https://cdn-icons.flaticon.com/png/512/4876/premium/4876935.png?token=exp=1642157486~hmac=485a5dfcff95699f66c0d383e77281fc" ><img src="icons/milk-creme-with-coffe.png" alt="milk-creme-with-coffe" width="100px" ></a><a title="milk-intier" href="https://cdn-icons-png.flaticon.com/512/373/373024.png" ><img src="icons/milk-entier.png" alt="milk-intier" width="100px" ></a><a title="milk-intier-with-coffe" href="https://cdn-icons-png.flaticon.com/512/3410/3410591.png" ><img src="icons/milk-intier-with-coffe.png" alt="milk-intier-with-coffe" width="100px" ></a><a title="owl" href="https://cdn-icons.flaticon.com/png/512/2987/premium/2987982.png?token=exp=1637681252~hmac=3bfa8602bc62a62b13a119816abe7189" ><img src="icons/owl.png" alt="owl" width="100px" ></a><a title="puberty" href="https://cdn-icons-png.flaticon.com/512/1184/1184428.png" ><img src="icons/puberty.png" alt="puberty" width="100px" ></a><a title="roast" href="https://cdn-icons.flaticon.com/png/512/3005/premium/3005169.png?token=exp=1642159283~hmac=4bf5c5bb69587469a2674d0a3c5ff830" ><img src="icons/roast.png" alt="roast" width="100px" ></a><a title="sadness" href="https://cdn-icons.flaticon.com/png/512/1650/premium/1650387.png?token=exp=1642157321~hmac=9a0192451d6a029bee48fefaea182d73" ><img src="icons/sadness.png" alt="sadness" width="100px" ></a><a title="social-media" href="https://cdn-icons.flaticon.com/png/512/1968/premium/1968750.png?token=exp=1642157257~hmac=9c89d34e6154b7ab87eab9e81b5bb295" ><img src="icons/social-media.png" alt="social-media" width="100px" ></a><a title="stomach-flu" href="https://cdn-icons-png.flaticon.com/512/4033/4033852.png" ><img src="icons/stomach-flu.png" alt="stomach-flu" width="100px" ></a><a title="sugar" href="https://cdn-icons.flaticon.com/png/512/2447/premium/2447720.png?token=exp=1642157213~hmac=a431c500494731d61c4a4850430957c3" ><img src="icons/sugar.png" alt="sugar" width="100px" ></a><a title="Sunset" href="https://cdn-icons-png.flaticon.com/512/1146/1146825.png" ><img src="icons/Sunset.png" alt="Sunset" width="100px" ></a><a title="tortellini" href="https://cdn-icons.flaticon.com/png/512/5620/premium/5620913.png?token=exp=1642159313~hmac=84038846a403d811441446769d9464ce" ><img src="icons/tortellini.png" alt="tortellini" width="100px" ></a><a title="coronavirus" href="https://cdn-icons-png.flaticon.com/512/2913/2913584.png" ><img src="icons/virus.png" alt="coronavirus" width="100px" ></a><a title="water-glass" href="https://cdn-icons.flaticon.com/png/512/1079/premium/1079119.png?token=exp=1642157839~hmac=a75edea22e27cba23327113fe68be18c" ><img src="icons/water-glass.png" alt="water-glass" width="100px" ></a><a title="wine" href="https://cdn-icons.flaticon.com/png/512/763/premium/763072.png?token=exp=1642157110~hmac=b41acfefde2fcd11cfc5fd27a85267aa" ><img src="icons/wine.png" alt="wine" width="100px" ></a><a title="yoghurt-20prc" href="https://cdn-icons.flaticon.com/png/512/4105/premium/4105244.png?token=exp=1642159344~hmac=713805e4bdd8a38758527943c95369e6" ><img src="icons/yogurt-20prc.png" alt="yoghurt-20prc" width="100px" ></a><a title="peas" href="https://cdn-icons-png.flaticon.com/512/5638/5638731.png" ><img src="icons/5638731_peas.png" alt="peas" width="100px" ></a>
</details>
<details>
<summary>
<h4>
Good Ware
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/good-ware" title="Good Ware">Good
Ware</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="traumatic" href="https://cdn-icons.flaticon.com/png/512/4200/premium/4200969.png?token=exp=1642157852~hmac=4e832dfda6a8e5f91becd872460768f8" ><img src="icons/traumatic.png" alt="traumatic" width="100px" ></a>
</details>
<details>
<summary>
<h4>
LAFS
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/lafs" title="LAFS">LAFS</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="ricotta" href="https://cdn-icons.flaticon.com/png/512/5713/premium/5713772.png?token=exp=1642159241~hmac=b0c9399b5b2e0be4cf1faf8b925b4695" ><img src="icons/ricotta.png" alt="ricotta" width="100px" ></a>
</details>
<details>
<summary>
<h4>
monkik
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/monkik" title="monkik">monkik</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="fish-n-chips" href="https://cdn-icons-png.flaticon.com/512/1365/1365578.png" ><img src="icons/1365578_fish-n-chips.png" alt="fish-n-chips" width="100px" ></a><a title="salmon" href="https://cdn-icons-png.flaticon.com/512/1728/1728736.png" ><img src="icons/1728736_salmon.png" alt="salmon" width="100px" ></a><a title="avocados" href="https://cdn-icons-png.flaticon.com/512/2503/2503796.png" ><img src="icons/2503796_avocados.png" alt="avocados" width="100px" ></a><a title="Thick soup" href="https://cdn-icons-png.flaticon.com/512/3823/3823394.png" ><img src="icons/3823394_thick-soup.png" alt="Thick soup" width="100px" ></a><a title="rib" href="https://cdn-icons-png.flaticon.com/512/776/776446.png" ><img src="icons/776446_rib.png" alt="rib" width="100px" ></a><a title="cake-slice" href="https://cdn-icons-png.flaticon.com/512/992/992717.png" ><img src="icons/cake-slice.png" alt="cake-slice" width="100px" ></a><a title="energyBar" href="https://cdn-icons-png.flaticon.com/512/1576/1576760.png" ><img src="icons/energyBar.png" alt="energyBar" width="100px" ></a><a title="soft-drinks" href="https://cdn-icons-png.flaticon.com/512/2405/2405479.png" ><img src="icons/soft-drinks.png" alt="soft-drinks" width="100px" ></a>
</details>
<details>
<summary>
<h4>
Pause08
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/pause08" title="Pause08">Pause08</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="coffe-cream" href="https://cdn-icons-png.flaticon.com/512/1929/1929028.png" ><img src="icons/coffe-cream.png" alt="coffe-cream" width="100px" ></a>
</details>
<details>
<summary>
<h4>
photo3idea
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/photo3idea-studio" title="photo3idea_studio">photo3idea_studio</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="yoghurt-zero" href="https://cdn-icons-png.flaticon.com/512/3829/3829580.png" ><img src="icons/3829580_yoghurt-zero.png" alt="yoghurt-zero" width="100px" ></a><a title="energyDrink" href="https://cdn-icons-png.flaticon.com/512/1472/1472941.png" ><img src="icons/energyDrink.png" alt="energyDrink" width="100px" ></a><a title="grow-up" href="https://cdn-icons-png.flaticon.com/512/4214/4214066.png" ><img src="icons/grow-up.png" alt="grow-up" width="100px" ></a>
</details>
<details>
<summary>
<h4>
pongsakornred
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/pongsakornred" title="pongsakornRed">pongsakornRed</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="party" href="https://cdn-icons-png.flaticon.com/512/864/864800.png" ><img src="icons/party.png" alt="party" width="100px" ></a>
</details>
<details>
<summary>
<h4>
popo2021
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/popo2021" title="popo2021">popo2021</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="Nectarine" href="https://cdn-icons.flaticon.com/png/512/5402/premium/5402243.png?token=exp=1638281299~hmac=521b8fa0a99649ba932995ef0f2c23e2" ><img src="icons/nectarine.png" alt="Nectarine" width="100px" ></a>
</details>
<details>
<summary>
<h4>
Roundicons
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/roundicons" title="Roundicons">Roundicons</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="percent" href="https://cdn-icons-png.flaticon.com/512/3012/3012388.png" ><img src="icons/3012388_percent.png" alt="percent" width="100px" ></a>
</details>
<details>
<summary>
<h4>
Smashicons
</h4>
</summary>

<div>

Icons made by
<a href="https://smashicons.com/" title="Smashicons">Smashicons</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="corn" href="https://cdn-icons-png.flaticon.com/512/1357/1357930.png" ><img src="icons/1357930_corn.png" alt="corn" width="100px" ></a><a title="Ravioli" href="https://cdn-icons-png.flaticon.com/512/2720/2720147.png" ><img src="icons/2720147_ravioli.png" alt="Ravioli" width="100px" ></a><a title="cans" href="https://cdn-icons.flaticon.com/png/512/3387/premium/3387264.png?token=exp=1642158641~hmac=aaadfb4efba29f23c5c10d42445a44a8" ><img src="icons/cans.png" alt="cans" width="100px" ></a><a title="chicken-noskin" href="https://cdn-icons.flaticon.com/png/512/3005/premium/3005149.png?token=exp=1642158793~hmac=4c1795ad2d29ab65fa05eeae71701f06" ><img src="icons/chicken-noskin.png" alt="chicken-noskin" width="100px" ></a><a title="coffee" href="https://cdn-icons.flaticon.com/png/512/2954/premium/2954820.png?token=exp=1642157964~hmac=5e587e082bd13a45e1fc54876e35b898" ><img src="icons/coffe.png" alt="coffee" width="100px" ></a><a title="fresh-juice" href="https://cdn-icons-png.flaticon.com/512/2738/2738730.png" ><img src="icons/fresh-juice.png" alt="fresh-juice" width="100px" ></a><a title="Fruit can" href="https://cdn-icons.flaticon.com/png/512/3387/premium/3387363.png?token=exp=1638455809~hmac=19743bf4a2d0bbc3c5eac465f89d9a40" ><img src="icons/fruit-can.png" alt="Fruit can" width="100px" ></a><a title="juice" href="https://cdn-icons.flaticon.com/png/512/3389/premium/3389046.png?token=exp=1642157512~hmac=a4a449259cda9bc8f54f839c2ad38105" ><img src="icons/juice.png" alt="juice" width="100px" ></a><a title="marijuana" href="https://cdn-icons-png.flaticon.com/512/2160/2160354.png" ><img src="icons/marijuana.png" alt="marijuana" width="100px" ></a><a title="mozarella" href="https://cdn-icons-png.flaticon.com/512/2427/2427036.png" ><img src="icons/mozzarella.png" alt="mozarella" width="100px" ></a><a title="sun" href="https://cdn-icons.flaticon.com/png/512/3558/premium/3558987.png?token=exp=1637681298~hmac=0a6cc7756ea4a8d356331864efde17d5" ><img src="icons/sun.png" alt="sun" width="100px" ></a><a title="waking-up" href="https://cdn-icons-png.flaticon.com/512/2228/2228219.png?token=exp=1637244704~hmac=becf15bfc69a1824bfa456969cd767f8" ><img src="icons/waking-up.png" alt="waking-up" width="100px" ></a>
</details>
<details>
<summary>
<h4>
turkkub
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/turkkub" title="turkkub">Smashicons</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="sardines" href="https://cdn-icons-png.flaticon.com/512/815/815958.png" ><img src="icons/815958_sardines.png" alt="sardines" width="100px" ></a>
</details>
<details>
<summary>
<h4>
Vitaly Gorbatchev
</h4>
</summary>

<div>

Icons made by
<a href="https://www.flaticon.com/authors/vitaly-gorbachev" title="Vitaly Gorbachev">Vitaly
Gorbachev</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>

</br><a title="Mandarine" href="https://cdn-icons.flaticon.com/png/512/1704/premium/1704398.png?token=exp=1642092349~hmac=f278db847a4fba3bd6aa421de91f49b9" ><img src="icons/1704398_mandarine.png" alt="Mandarine" width="100px" ></a><a title="cassis" href="https://cdn-icons-png.flaticon.com/512/2044/2044988.png" ><img src="icons/2044988_cassis.png" alt="cassis" width="100px" ></a><a title="aperitive" href="https://cdn-icons.flaticon.com/png/512/3086/premium/3086535.png?token=exp=1642158000~hmac=12e95df177348b7c1ea3476d61f4441c" ><img src="icons/aperitive.png" alt="aperitive" width="100px" ></a>
</details>
</details>
<details>
<summary>
<h4>
GIFs
</h4>
</summary>
<details>
<summary>
<h4>
NASA
</h4>
</summary>

<div>

Astronaut GIFs made by
<a href="https://giphy.com/nasa/react-like-a-nasa-astronaut" >NASA</a>
on <a href="https://giphy.com/">GIPHY</a>.

</div>

</br><a title="Dab-on-ya-outer-space" href="https://i.giphy.com/media/J1WyRIJrTeKs6R6VHL/giphy.webp" ><img src="icons/Dab-on-ya-outer-space.webp" alt="Dab-on-ya-outer-space" width="100px" ></a><a title="Heavy-metal-space " href="https://i.giphy.com/media/KazKNpwOrLmngkbme0/giphy.webp" ><img src="icons/heavy-metal-space.webp" alt="Heavy-metal-space " width="100px" ></a><a title="Moon-walk-austronaut" href="https://i.giphy.com/media/lNLPKnfJU5YWz1pO0v/giphy.webp" ><img src="icons/moon-walk-dance.webp" alt="Moon-walk-austronaut" width="100px" ></a><a title="On-my-way-austronaut" href="https://i.giphy.com/media/fUH5dmsC028dTvmKyD/giphy.webp" ><img src="icons/on-my-way-austronaut.webp" alt="On-my-way-austronaut" width="100px" ></a><a title="outer-space-applause" href="https://i.giphy.com/media/dUHRLc8htPBIyMheru/giphy.webp" ><img src="icons/Outer-Space-Applause.webp" alt="outer-space-applause" width="100px" ></a><a title="Outer-space-clap" href="https://i.giphy.com/media/MByJoGjKsgwBj8kpXL/giphy.webp" ><img src="icons/outer-space-clap.webp" alt="Outer-space-clap" width="100px" ></a><a title="Outer-space-dance " href="https://i.giphy.com/media/S99cgkURVO62qemEKM/200w.webp" ><img src="icons/Outer-space-dance.webp" alt="Outer-space-dance " width="100px" ></a><a title="Outer-space-thumbs-up" href="https://i.giphy.com/media/MAzunB1Ru6zAYlYgPD/giphy.webp" ><img src="icons/outer-space-thumbs.webp" alt="Outer-space-thumbs-up" width="100px" ></a><a title="Outer-space-yes" href="https://i.giphy.com/media/KEG5UtvXUD7WPIhhuy/giphy.webp" ><img src="icons/Outer-space-yes.webp" alt="Outer-space-yes" width="100px" ></a><a title="Tired-outer-space" href="https://i.giphy.com/media/W69PyYABOmgAS0GaTk/giphy.webp" ><img src="icons/Tired-outer-space.webp" alt="Tired-outer-space" width="100px" ></a><a title="Well-done-good-job" href="https://i.giphy.com/media/MXWvmQ0dWxqEOLIzqu/giphy.webp" ><img src="icons/Well-done-good-job.webp" alt="Well-done-good-job" width="100px" ></a>
</details>
</details>
